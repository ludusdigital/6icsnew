/* CURSOR STYLIZE */
$("body").niceScroll({
    cursorcolor: "black",
    cursorwidth: "5px",
    cursorborder: "1px solid grey"
});

$(".products-side-wrapper").niceScroll({
    cursorcolor: "black",
    cursorwidth: "5px",
    cursorborder: "1px solid grey"
});
$(".bag-section").niceScroll({
    cursorcolor: "black",
    cursorwidth: "5px",
    cursorborder: "1px solid grey"
});

$(".sizes-wrapper, .colors-wrapper").niceScroll({
    cursorcolor: "black",
    cursorwidth: "5px",
    cursorborder: "1px solid grey"
});

$(document).ready(function () {

    /* MENU FUNCIONALITY */
    var $menuBtn = $('.nav-menu-btn');
    var $shopBtn = $('.shop-btn');
    var $searchBtn = $('.search-btn');
    var $loginBtn = $('.login-btn');
    var $sizingBtn = $('.sizing-info-btn');
    var $shippingBtn = $('.shipping-info-btn');
    var $bagBtn = $('.bag-btn');
    var $filterBtn = $('.filter-btn');
    var $mainBanner = $('.main-banner');
    var $menuCloseBtn = $('.menu-modal-close-btn');
    var $productsSectionCloseBtn = $('.products-section-close-btn');
    var $searchSectionCloseBtn = $('.search-section-close-btn');
    var $loginSectionCloseBtn = $('.login-section-close-btn');
    var $bagSectionCloseBtn = $('.bag-section-close-btn');
    var $filterSectionCloseBtn = $('.filter-section-close-btn');
    var $sizingSectionCloseBtn = $('.sizing-info-section-close-btn');
    var $shippingSectionCloseBtn = $('.shipping-info-section-close-btn');
    var $menuItem = $('.menu-item');
    var $menuModal = $('.menu-modal-wrapper');
    var $productsSection = $('.products-side-section');
    var $searchSection = $('.search-section');
    var $sizingSection = $('.sizing-info-section');
    var $shippingSection = $('.shipping-info-section');
    var $bagSection = $('.bag-section');
    var $loginSection = $('.login-section');
    var $filterSection = $('.filter-side-wrapper');
    var $menuTween = new TimelineMax();
    var $tweenProductsSide = new TimelineMax();

    $menuBtn.click(function () {
        $menuTween.to($menuModal, 0.3, {
            display: "block",
            autoAlpha: 1
        }).staggerTo($menuItem, 0.5, {
            y: -50,
            autoAlpha: 1,
            ease: Power1.easeInOut
        }, 0.2);
    });
    $menuCloseBtn.click(function () {
        $menuTween.to($menuModal, 0.3, {
            display: "none",
            autoAlpha: 0
        }).to($menuItem, 0, {
            autoAlpha: 0
        }).to($menuItem, 0, {
            y: 50
        });
    });

    //BANNER ANIMA
    TweenLite.to($mainBanner, 2, {
        height: "200px",
        ease: Expo.easeOut,
        delay: 0.5
    });

    //SEARCH BTN
    $searchBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($searchSection, 0.5, {
            top: "0px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        //CLOSE MENU MODAL
        TweenLite.to($menuModal, 0.3, {
            display: "none",
            autoAlpha: 0
        });
    });

    $searchBtn.click(function (e) {
        e.preventDefault();
        $('#search-field').focus();
    });

    $searchSection.click(function (e) {
        e.stopPropagation();
    });


    $searchSectionCloseBtn.click(function (e) {
        TweenLite.to($searchSection, 0.5, {
            top: "-150px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        TweenLite.to($searchSection, 0.5, {
            top: "-150px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    //BAG SECTION
    $bagBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($bagSection, 1, {
            display: "block",
            top: "0%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
    });

    $bagSection.click(function (e) {
        e.stopPropagation();
    });

    $bagSectionCloseBtn.click(function (e) {
        TweenLite.to($bagSection, 1, {
            top: "-100%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        TweenLite.to($bagSection, 1, {
            top: "-100%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    //SIZING SECTION
    $sizingBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($sizingSection, 1, {
            display: "block",
            top: "0%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
    });

    $sizingSection.click(function (e) {
        e.stopPropagation();
    });

    $sizingSectionCloseBtn.click(function (e) {
        var $sectionHeight = -$sizingSection.innerHeight();
        TweenLite.to($sizingSection, 1, {
            top: $sectionHeight,
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        var $sectionHeight = -$sizingSection.innerHeight();
        TweenLite.to($sizingSection, 1, {
            top: $sectionHeight,
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    //SHIPPING SECTION
    $shippingBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($shippingSection, 1, {
            display: "block",
            top: "0%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
    });

    $shippingSection.click(function (e) {
        e.stopPropagation();
    });

    $shippingSectionCloseBtn.click(function (e) {
        var $sectionHeight = -$shippingSection.innerHeight();
        TweenLite.to($shippingSection, 1, {
            top: $sectionHeight,
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        var $sectionHeight = -$shippingSection.innerHeight();
        TweenLite.to($shippingSection, 1, {
            top: $sectionHeight,
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    //LOGIN SECTION
    $loginBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($loginSection, 1, {
            display: "block",
            top: "0%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        //CLOSE MENU MODAL
        TweenLite.to($menuModal, 0.3, {
            display: "none",
            autoAlpha: 0
        });
    });

    $loginSection.click(function (e) {
        e.stopPropagation();
    });

    $loginSectionCloseBtn.click(function (e) {
        TweenLite.to($loginSection, 2, {
            top: "-100%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        TweenLite.to($loginSection, 2, {
            top: "-100%",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    //SHOP BTN
    $shopBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($productsSection, 0.5, {
            left: "0px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        TweenLite.to(window, 1, {
            scrollTo: 0
        });
        //PRODUCTS SIDE ANIMA
        $tweenProductsSide.staggerFrom($(".side-section-product"), 0.7, {
            x: -300,
            ease: Expo.easeOut
        }, 0.1);
    });

    $productsSectionCloseBtn.click(function () {
        TweenLite.to($productsSection, 0.5, {
            left: "-300px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $productsSection.click(function (e) {
        e.stopPropagation();
    });

    $('body,html').click(function (e) {
        TweenLite.to($productsSection, 0.5, {
            left: "-300px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    /* FILTER SIDE SECTION */
    $filterBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($filterSection, 0.5, {
            left: "0px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        //SCROLL TO TOP
        TweenLite.to(window, 0.5, {
            scrollTo: 0
        });
    });

    $filterSection.click(function (e) {
        e.stopPropagation();
    });

    $filterSectionCloseBtn.click(function (e) {
        TweenLite.to($filterSection, 0.5, {
            left: "-120px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $('body,html').click(function (e) {
        TweenLite.to($filterSection, 0.5, {
            left: "-120px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });


    //FITLER CLICK FUNCTIONALITY

    //toggle function
    (function ($) {
        $.fn.clickToggle = function (func1, func2) {
            var funcs = [func1, func2];
            this.data('toggleclicked', 0);
            this.click(function () {
                var data = $(this).data();
                var tc = data.toggleclicked;
                $.proxy(funcs[tc], this)();
                data.toggleclicked = (tc + 1) % 2;
            });
            return this;
        };
    }(jQuery));

    var $filterColor = $('.filter-colors .color');
    var $filterSize = $('.filter-sizes .size');

    $filterColor.clickToggle(function () {
        $(this).addClass('active-color-filter');
    }, function () {
        $(this).removeClass('active-color-filter');
    });

    $filterSize.clickToggle(function () {
        $(this).addClass('active-size-filter');
    }, function () {
        $(this).removeClass('active-size-filter');
    });


    /* MENU IMAGE HOVER FUNCIONALITY */
    $('.menu-nav-list').children('li').hover(function () {
        var index = $(this).index();
        console.log(index);
        $(".nav-img-list li").eq(index).addClass("open-img-menu");
    }, function () {
        var index = $(this).index();
        $(".nav-img-list li").eq(index).removeClass("open-img-menu");
    });

    /* BAG QUANTITY FUNCTIONALITY */

    $(".qty-btn").on("click", function () {

        var $button = $(this);
        var oldValue = $button.closest('.product-quantity').find("input.quantity-input").val();
        if ($button.text() == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
                $(this).parents('li').fadeOut();
            }

        }
        // Delete item

        $button.closest('.product-quantity').find("input.quantity-input").val(newVal);

    });

    /* PRODUCT PAGE HIDE PRODUCT NAME AND FUNCT */
    var $productHeaderName = $('.product-header-title');
    //var $productFuncWrapepr = $('.product-info-func-wrapper');
    var $hideElement = new TimelineMax();

    var ctrl = new ScrollMagic.Controller({
        globalSceneOptions: {
            triggerHook: 'onEnter',
            offset: 10
        }
    });

    var seeFooterHideProductElem = new ScrollMagic.Scene({
            triggerElement: "#full-description"
        })
        .setClassToggle('.product-header-title, .product-info-func-wrapper, .sizes-wrapper, .colors-wrapper, .products-side-section', 'fade-element')
        .addTo(ctrl);

    /* PRODUCT PAGE COLORS FUNCIONALITY */
    var $productChooseColorBtn = $('.choose-color-btn');
    var $productChooseSizeBtn = $('.choose-size-btn');
    var $productChooseSizeSection = $('.sizes-wrapper');
    var $productChooseColorsSection = $('.colors-wrapper');

    $productChooseColorBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($productChooseColorsSection, 0.5, {
            right: "0px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        TweenLite.to($productChooseSizeSection, 0.5, {
            right: "-70px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $productChooseColorsSection.click(function (e) {
        e.stopPropagation();
    });

    $('body,html').click(function (e) {
        TweenLite.to($productChooseColorsSection, 0.5, {
            right: "-70px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $productChooseSizeBtn.click(function (e) {
        e.stopPropagation();
        TweenLite.to($productChooseSizeSection, 0.5, {
            right: "0px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0.1)",
            ease: Expo.easeOut
        });
        TweenLite.to($productChooseColorsSection, 0.5, {
            right: "-70px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    $productChooseSizeSection.click(function (e) {
        e.stopPropagation();
    });

    $('body,html').click(function (e) {
        TweenLite.to($productChooseSizeSection, 0.5, {
            right: "-70px",
            boxShadow: "0px 0px 33px rgba(0, 0, 0, 0)",
            ease: Expo.easeOut
        });
    });

    /* SHARE BTN FUNCTIONALITY */
    var $sharePageBtn = $('.share-page-btn');
    var $shareTween = new TimelineMax();

    $sharePageBtn.clickToggle(function () {
        $shareTween.to($('.share-btn-link'), 0.3, {
            display: "inline-block"
        }).staggerTo($('.share-btn-link'), 0.5, {
            autoAlpha: 1,
            ease: Expo.easeOut
        }, 0.05);
    }, function () {
        $shareTween.to($('.share-btn-link'), 0.3, {
            autoAlpha: 0,
            ease: Expo.easeOut,
            display: "none"
        });
    });

    /* $sharePageBtn.click(function () {
        $shareTween.staggerTo($('.share-btn-link'),0.3, {
            display: "inline-block",
            autoAlpha: 1,
            ease: Expo.easeOut
        }, 0.05);
    }); */

    /* ADD TO CART MENU ICON */
    var $addToCartBtn = $('.add-to-cart-btn');
    var $bagIconTween = new TimelineMax();
    $addToCartBtn.click(function () {
        $('.bag-btn .icon-bag').addClass('fade-element');
        $bagIconTween.to($('.products-number-bag'), 0, {
            display: "block",
            autoAlpha: 1,
        }).from($('.products-number-bag'), 0.3, {
            scale: 0,
            ease: Expo.easeOut
        }).from($('.products-number-bag p'), 0.5, {
            y: 5,
            autoAlpha: 0,
            ease: Expo.easeOut
        });
    });

    /* SELECT LINKS WITH HASHES */
    var $fullDescriptionBtn = $('.more-info-btn')
    $fullDescriptionBtn.click(function () {
        TweenLite.to(window, 1, {
            scrollTo: "#full-description"
        });
    });

    /* PRODUCT PAGE GRID SWITCH */
    var $switch3ColBtn = $('.row-3-btn');
    var $switch4ColBtn = $('.row-4-btn');
    var $gridWrapper = $('.grid-wrapper');
    $switch3ColBtn.click(function () {
        $gridWrapper.addClass('grid-3');
        $(this).addClass('active');
        $switch4ColBtn.removeClass('active');
    });
    $switch4ColBtn.click(function () {
        $gridWrapper.removeClass('grid-3');
        $(this).addClass('active');
        $switch3ColBtn.removeClass('active');
    });

    /* COPY TO CLIPBOARD */
    var clipboard = new Clipboard('.link-copy-btn', {
        text: function () {
            return document.querySelector('input[type=hidden]').value;
        }
    });
    clipboard.on('success', function (e) {
        //alert("Copied!");
        e.clearSelection();
    });
    $("#input-url-copy").val(location.href);
    //safari
    if (navigator.vendor.indexOf("Apple") == 0 && /\sSafari\//.test(navigator.userAgent)) {
        $('.link-copy-btn').on('click', function () {
            var msg = window.prompt("Copy this link", location.href);

        });
    }






});