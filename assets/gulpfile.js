var gulp = require('gulp'),
  connect = require('gulp-connect'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('gulp-autoprefixer'),
  sass = require('gulp-sass'),
  gutil = require('gulp-util');

var jsLibs = [
  'bower_components/jquery/dist/jquery.js',
  'bower_components/bootstrap/dist/js/bootstrap.js',
  'bower_components/gsap/src/uncompressed/TweenMax.js',
  'bower_components/gsap/src/uncompressed/plugins/ScrollToPlugin.js',
  'bower_components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js',
  'animation.gsap.min.js',
  'vendor/jquery.nicescroll.js'

];

var sassPaths = [
  'bower_components/bootstrap/dist/css',
  'vendor/',
  'scss/'
];

gulp.task('connect', function () {
  connect.server({
    root: '../',
    port: 8000,
    livereload: true
  });
});


gulp.task('sass', function () {
  return gulp.src('scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
        includePaths: sassPaths,
        outputStyle: 'compressed'
      })
      .on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions', 'ie >= 9']
    }))
    .pipe(sourcemaps.write('maps'))
    .pipe(gulp.dest('../css'))
    .pipe(connect.reload());
});


gulp.task('scripts', function () {
  return gulp.src(jsLibs)
    .pipe(uglify())
    .pipe(sourcemaps.init())
    .pipe(concat('../js/vendor.min.js'))
    .pipe(sourcemaps.write('../js/'))
    .on('error', gutil.log)
    .pipe(gulp.dest('../js'))
    .pipe(connect.reload());
});

gulp.task('html', function () {
  gulp.src('../*.html')
    .pipe(connect.reload());
});

gulp.task('jswatch', function () {
  gulp.watch(['../js/main.js'], ['scripts']);
});

gulp.task('htmlwatch', function () {
  gulp.watch(['../*.html'], ['html']);
});

gulp.task('sasswatch', function () {
  gulp.watch(['scss/**/*.scss'], ['sass']);
});

gulp.task('default', ['connect', 'htmlwatch', 'sasswatch', 'jswatch']);
